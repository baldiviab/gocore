package gocore

import (
	"errors"
	"sort"
)

type Board struct {
	Xdim       int
	Hcap       int
	LastStates [][]Player
	State      []Player
	History    []Move
}

type Move struct {
	Player  Player
	Index   int
	Pass    bool
	Forfeit bool
}

type Group struct {
	Player    Player
	Members   []int
	Liberties []int
}

type Player int

func (p *Player) String() string {
	pv := *p
	switch {
	case pv == EMPTY:
		return "EMPTY"
	case pv == BLACK:
		return "BLACK"
	case pv == WHITE:
		return "WHITE"
	default:
		return "INVALID"
	}
}

const (
	EMPTY = Player(iota)
	BLACK
	WHITE
)

var (
	ErrInvalidDim            = errors.New("Invalid dimension value")
	ErrInvalidHcap           = errors.New("Invalid handicap value")
	ErrInvalidPv             = errors.New("Invalid player value")
	ErrInvalidPos            = errors.New("Invalid position")
	ErrInvalidMovePlayer     = errors.New("Not this players turn")
	ErrInvalidMovePos        = errors.New("Position is not empty")
	ErrInvalidMoveSurrounded = errors.New("Invalid move: surrounded")
	ErrGameCompleted         = errors.New("Game is already completed")
)

func NewBoard(xdim int, hcap int) (*Board, error) {
	if 9 > xdim || xdim > 23 {
		return nil, ErrInvalidDim
	}
	if 0 > hcap || hcap > 12 {
		return nil, ErrInvalidHcap
	}

	s := make([]Player, xdim*xdim)
	h := []Move{}

	return &Board{xdim, hcap, nil, s, h}, nil
}

func (b *Board) Move(p Player, idx int) error {
	cp, err := b.CurrentTurn()
	if err != nil {
		return err
	}

	if p != BLACK && p != WHITE {
		return ErrInvalidPv
	}
	if p != cp {
		return ErrInvalidMovePlayer
	}
	if err := b.validateIndex(idx); err != nil {
		return err
	}

	if b.State[idx] != EMPTY {
		return ErrInvalidMovePos
	}

	// Check for dead groups
	// Error ignored, index already validated
	a, _ := b.getAdjacent(idx)

	needCap := true
	caps := []Group{}
	for _, aIdx := range a {
		idxP := b.State[aIdx]
		if idxP == EMPTY {
			needCap = false
			break
		} else if idxP == p {
			// getAdjacent can't provide an invalid index
			g, _ := b.GetGroup(aIdx)
			if len(g.Liberties) > 1 {
				needCap = false
				break
			}
		} else {
			// getAdjacent can't provide an invalid index
			g, _ := b.GetGroup(aIdx)
			if len(g.Liberties) == 1 {
				caps = append(caps, *g)
			}
		}
	}
	if needCap && len(caps) == 0 {
		return ErrInvalidMoveSurrounded
	}

	for i := 0; i < len(caps); i++ {
		for j := 0; j < len(caps[i].Members); j++ {
			b.State[caps[i].Members[j]] = EMPTY
		}
	}

	m := Move{
		Player: p,
		Index:  idx,
	}
	b.State[idx] = p
	b.History = append(b.History, m)
	return nil
}

func (b *Board) CurrentTurn() (Player, error) {
	if b.Completed() {
		return Player(0), ErrGameCompleted
	}
	var turn = len(b.History)
	if turn <= b.Hcap {
		return BLACK, nil
	}
	return Player(1 + (turn-b.Hcap)%2), nil
}

func (b *Board) Pass() error {
	p, err := b.CurrentTurn()
	if err != nil {
		return err
	}

	b.History = append(b.History, Move{
		Player: p,
		Pass:   true,
	})
	return nil
}

func (b *Board) Forfeit(p Player) error {
	if b.Completed() {
		return ErrGameCompleted
	}
	if p != BLACK && p != WHITE {
		return ErrInvalidPv
	}
	b.History = append(b.History, Move{
		Player:  p,
		Forfeit: true,
	})
	return nil
}

func (b *Board) Completed() bool {
	hlen := len(b.History)
	if hlen > 0 {
		if b.History[hlen-1].Forfeit {
			return true
		}
	}
	if hlen > 2 {
		return b.History[hlen-1].Pass && b.History[hlen-3].Pass
	}
	return false
}

func (b *Board) validateIndex(idx int) error {
	if 0 > idx || idx >= b.Xdim*b.Xdim {
		return ErrInvalidPos
	}
	return nil
}

func (b *Board) getAdjacent(idx int) ([]int, error) {
	n := []int{}
	mod := idx % b.Xdim

	if err := b.validateIndex(idx); err != nil {
		return nil, err
	}

	if idx >= b.Xdim {
		n = append(n, idx-b.Xdim)
	}

	if mod > 0 {
		n = append(n, idx-1)
	}

	if mod < b.Xdim-1 {
		n = append(n, idx+1)
	}

	if idx < (b.Xdim * (b.Xdim - 1)) {
		n = append(n, idx+b.Xdim)
	}

	return n, nil
}

func (b *Board) GetGroup(idx int) (*Group, error) {
	if err := b.validateIndex(idx); err != nil {
		return nil, err
	}
	p := b.State[idx]
	if p == EMPTY {
		return &Group{EMPTY, nil, nil}, nil
	}

	inGroup := make(map[int]struct{})
	group := []int{idx}
	inGroup[idx] = struct{}{}
	liberties := make(map[int]struct{})
	libSet := []int{}

	for i := 0; i < len(group); i++ {
		/* Error ignored
		 * Only an invalid index error can occur which was checked
		 */
		a, _ := b.getAdjacent(group[i])
		for _, aidx := range a {
			ap := b.State[aidx]
			if ap == p {
				if _, ok := inGroup[aidx]; !ok {
					group = append(group, aidx)
					inGroup[aidx] = struct{}{}
				}
			} else if ap == EMPTY {
				if _, ok := liberties[aidx]; !ok {
					liberties[aidx] = struct{}{}
				}
			}
		}
	}

	for k, _ := range liberties {
		libSet = append(libSet, k)
	}

	sort.Ints(group)
	sort.Ints(libSet)
	return &Group{p, group, libSet}, nil
}
