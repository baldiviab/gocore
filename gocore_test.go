package gocore

import (
	"testing"
)

func TestPlayer(t *testing.T) {
	tests := []struct {
		p      Player
		expect string
	}{
		{Player(0), "EMPTY"},
		{Player(1), "BLACK"},
		{Player(2), "WHITE"},
		{Player(-1), "INVALID"},
		{Player(3), "INVALID"},
	}

	for i, test := range tests {
		actual := test.p.String()
		if actual != test.expect {
			t.Error("Test: ", i, " Expected: ", test.expect, " Got: ", actual)
		}
	}
}

func TestCompleted(t *testing.T) {
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	if b.Completed() {
		t.Error("Expected new board not completed")
		return
	}
	err = b.Forfeit(BLACK)
	if err != nil {
		t.Error("Expected no error on forfeit")
		return
	}
	if !b.Completed() {
		t.Error("Expected forfeited game completed")
		return
	}

	b, err = NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	moveSet := []struct {
		p    Player
		idx  int
		pass bool
	}{
		{BLACK, 0, true},
		{WHITE, 0, false},
		{BLACK, 0, true},
	}
	for _, move := range moveSet {
		if move.pass {
			b.Pass()
		} else {
			b.Move(move.p, move.idx)
		}
	}
	if !b.Completed() {
		t.Error("Expected 2 passes game completed")
		t.Error(b)
		return
	}
}

func TestPass(t *testing.T) {
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	tests := []struct {
		expectErr error
	}{
		{nil},
		{nil},
		{nil},
		{ErrGameCompleted},
	}

	for i, test := range tests {
		err := b.Pass()
		if err != test.expectErr {
			t.Error("Expected test: ", i, test.expectErr, " Got: ", err)
		}
	}
}

func TestCurrentTurn(t *testing.T) {
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	turn, err := b.CurrentTurn()
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	if turn != BLACK {
		t.Error("Expected turn: ", BLACK, " Got: ", turn)
	}
	b.Move(BLACK, 0)
	turn, err = b.CurrentTurn()
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	if turn != WHITE {
		t.Error("Expected turn: ", WHITE, " Got: ", turn)
	}
	b.Forfeit(BLACK)
	turn, err = b.CurrentTurn()
	if err != ErrGameCompleted {
		t.Error("Expected error: ", ErrGameCompleted, " Got: ", err)
	}
}

func TestForfeit(t *testing.T) {
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	tests := []struct {
		p         Player
		expectErr error
	}{
		{Player(-1), ErrInvalidPv},
		{BLACK, nil},
		{BLACK, ErrGameCompleted},
	}
	for i, test := range tests {
		err := b.Forfeit(test.p)
		if err != test.expectErr {
			t.Error("test: ", i, " expected: ", test.expectErr, " Got: ", err)
		}
	}
}

func TestNewBoard(t *testing.T) {
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	if b.Xdim != 19 {
		t.Error("Expected Xdim to be 19. Got: ", b.Xdim)
	}
	if b.Hcap != 0 {
		t.Error("Expected Hcap to be 0. Got: ", b.Hcap)
	}
	if b.Hcap != 0 {
		t.Error("Expected Hcap to be 0. Got: ", b.Hcap)
	}
	if len(b.State) != 19*19 {
		t.Error("Board State is incorrect length. Expected: ", 19*19, " Got: ", len(b.State))
	}
	if len(b.History) != 0 {
		t.Error("Board History is incorrect length. Expected: ", 0, " Got: ", len(b.History))
	}

	if _, err = NewBoard(8, 0); err != ErrInvalidDim {
		t.Error("Expected: ", ErrInvalidDim, " Got: ", err)
	}

	if _, err = NewBoard(24, 0); err != ErrInvalidDim {
		t.Error("Expected: ", ErrInvalidDim, " Got: ", err)
	}

	if _, err = NewBoard(9, -1); err != ErrInvalidHcap {
		t.Error("Expected: ", ErrInvalidHcap, " Got: ", err)
	}

	if _, err = NewBoard(9, 13); err != ErrInvalidHcap {
		t.Error("Expected: ", ErrInvalidHcap, " Got: ", err)
	}

	if _, err = NewBoard(23, 12); err != nil {
		t.Error("Expected: ", nil, " Got: ", err)
	}

}

func TestMove(t *testing.T) {
	tests := []struct {
		p      Player
		idx    int
		expect error
	}{
		{EMPTY, 0, ErrInvalidPv},
		{WHITE, 0, ErrInvalidMovePlayer},
		{Player(3), 0, ErrInvalidPv},
		{BLACK, -1, ErrInvalidPos},
		{BLACK, 500, ErrInvalidPos},
		{BLACK, 0, nil},
		{BLACK, 0, ErrInvalidMovePlayer},
		{BLACK, 1, ErrInvalidMovePlayer},
		{WHITE, 0, ErrInvalidMovePos},
		{WHITE, 1, nil},
		{BLACK, 2, nil},
		{WHITE, 19, nil},
		{BLACK, 0, ErrInvalidMoveSurrounded},
		{BLACK, 3, nil},
		{WHITE, 0, nil},
	}
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}

	for i, test := range tests {
		if err = b.Move(test.p, test.idx); err != test.expect {
			t.Error("Test: ", i, " Expected: ", test.expect, " Got: ", err)
			t.Error(b)
		}
	}
	err = b.Forfeit(BLACK)
	if err != nil {
		t.Error("Expected Error: nil Got: ", err)
	}
	err = b.Move(BLACK, 0)
	if err != ErrGameCompleted {
		t.Error("Expected Error: ", ErrGameCompleted, " Got: ", err)
	}
}

func TestGetAdjacent(t *testing.T) {
	tests := []struct {
		idx       int
		expect    []int
		expectErr error
	}{
		{361, nil, ErrInvalidPos},
		{-1, nil, ErrInvalidPos},
		{0, []int{1, 19}, nil},
		{18, []int{17, 37}, nil},
		{342, []int{323, 343}, nil},
		{360, []int{341, 359}, nil},
		{190, []int{171, 191, 209}, nil},
		{208, []int{189, 207, 227}, nil},
		{10, []int{9, 11, 29}, nil},
		{351, []int{332, 350, 352}, nil},
	}
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}

	for i, test := range tests {
		setFail := false
		actual, err := b.getAdjacent(test.idx)
		if err != nil {
			if err != test.expectErr {
				t.Error("Test:", i, " Expected error: ", test.expectErr, "Got: ", err)
			}
			continue
		}
		if len(actual) != len(test.expect) {
			t.Error("Test:", i, " Expected Len: ", len(test.expect))
			setFail = true
		}
		exists := make(map[int]struct{})
		for _, v := range actual {
			exists[v] = struct{}{}
		}
		for _, v := range test.expect {
			if _, ok := exists[v]; !ok {
				t.Error("Test:", i, " Expected idx:", test.idx, "neighbors to contain: ", v)
				setFail = true
			}
		}
		if setFail {
			t.Error("Actual Set: ", actual)
		}
	}
}

func TestGetGroup(t *testing.T) {
	b, err := NewBoard(19, 0)
	if err != nil {
		t.Error("Expected error: ", nil, " Got: ", err)
	}
	moveSet := []struct {
		p   Player
		idx int
	}{
		{BLACK, 1},
		{WHITE, 2},
		{BLACK, 0},
		{WHITE, 18},
		{BLACK, 45},
		{WHITE, 17},
		{BLACK, 52},
		{WHITE, 37},
	}

	for _, move := range moveSet {
		err := b.Move(move.p, move.idx)
		if err != nil {
			t.Error("Expected error: ", nil, " Got: ", err)
			return
		}
	}

	tests := []struct {
		idx       int
		members   []int
		liberties []int
		expectErr error
	}{
		{-1, []int{0, 1}, nil, ErrInvalidPos},
		{500, []int{0, 1}, nil, ErrInvalidPos},
		{0, []int{0, 1}, []int{19, 20}, nil},
		{1, []int{0, 1}, []int{19, 20}, nil},
		{2, []int{2}, []int{3, 21}, nil},
		{18, []int{17, 18, 37}, []int{16, 36, 56}, nil},
		{3, nil, nil, nil},
	}

	for i, test := range tests {
		failSet := false
		actual, err := b.GetGroup(test.idx)
		if err != nil {
			if err != test.expectErr {
				t.Error("Expected error: ", nil, " Got: ", err)
			}
			continue
		}

		// Check members
		if len(test.members) != len(actual.Members) {
			t.Error("Test: ", i, " Expected len: ", len(test.members), " Got: ", len(actual.Members))
			failSet = true
		}
		exist := make(map[int]struct{})
		for _, v := range actual.Members {
			exist[v] = struct{}{}
		}
		for _, v := range test.members {
			if _, ok := exist[v]; !ok {
				t.Error("Test: ", i, " Expected ", v, " to expect")
				failSet = true
			}
		}

		// Check liberties
		if len(test.liberties) != len(actual.Liberties) {
			t.Error("Test: ", i, " Expected liberties len: ", len(test.liberties), " Got: ", len(actual.Liberties))
			failSet = true
		}
		exist = make(map[int]struct{})
		for _, v := range actual.Liberties {
			exist[v] = struct{}{}
		}
		for _, v := range test.liberties {
			if _, ok := exist[v]; !ok {
				t.Error("Test: ", i, " Expected liberties to contain:", v)
				failSet = true
			}
		}

		if failSet {
			t.Error(actual)
		}
	}
}
